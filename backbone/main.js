var Model = Backbone.Model;
var Collection = Backbone.Collection;
var View = Backbone.View;

var ENTER_KEY = 13;

function template(selector) {
  return _.template($(selector).html());
}

var TodoModel = Model.extend({
  defaults: {
    text: '',
    done: false
  }
});

var TodoCollection = Collection.extend({
  model: TodoModel
});

var TodoItem = View.extend({
  template: template('#todo-item-template'),
  className: 'todo-item',
  events: {
    'change [role=done-check]': 'setChecked'
  },
  initialize: function() {
  },
  setChecked: function() {
    this.model.set('done', this.$doneCheck.prop('checked'));
  },
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    this.$doneCheck = this.$('[role=done-check]');
  }
});

var TodoList = View.extend({
  className: 'todo-list',
  initialize: function() {
    this.listenTo(this.collection, 'all', this.render);
  },
  renderOne: function(item) {
    var view = new TodoItem({model: item});
    this.$el.append(view.$el);
    view.render();
  },
  render: function() {
    this.$el.empty();
    this.collection.each(this.renderOne, this);
  }
});

var App = View.extend({
  template: template('#app-template'),
  className: 'app',
  events: {
    'keyup [role=todo-input]': 'processKeyup',
    'click [role=clear-done]': 'clearDone'
  },
  initialize: function() {
    this.collection = new TodoCollection();
    this.todoList = new TodoList({
      collection: this.collection
    });
  },
  linkElements: function() {
    this.$todoInput = this.$('[role=todo-input]');
  },
  processKeyup: function(event) {
    if (event.which === ENTER_KEY) {
      this.collection.add({
        text: this.$todoInput.val()
      });
      this.$todoInput.val('');
    }
  },
  render: function() {
    this.$el.html(this.template());
    this.linkElements();

    this.$('[role=list-container]').html(this.todoList.$el);
  },
  clearDone: function() {
    this.collection.each(function(item) {
      if (item && item.get('done')) {
        this.collection.remove(item);
      }
    }, this);
  }
});

$(function() {
  var app = new App({
    el: $('body')
  });

  app.render();
});
