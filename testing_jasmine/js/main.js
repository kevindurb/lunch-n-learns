function add(a, b) {
  return a + b;
}

function badDomInteractionTest(text) {
  $('body').append('this is the text: ' + text);
}

function betterTextAppendTest(text) {
  return 'this is the text: ' + text;
}

function appendTextToBody(text) {
  $('body').append(text);
}
