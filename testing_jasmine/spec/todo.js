describe('a todo item', function() {
  it('should have sane defaults', function() {
    var item = new TodoModel();
    expect(item.get('done')).toBe(false);
    expect(item.get('text')).toBe('');
  });
});
