describe('a suite', function() {
  it('should pass', function() {
    expect(true).toBe(true);
  });
});

describe('an add function', function() {
  it('should be able to add two positive numbers together', function() {
    var a = 5;
    var b = 4;
    var result = add(a, b);
    expect(result).toBe(9);
  });

  it('should be able to add two negative numbers', function() {
    var a = -5;
    var b = -4;
    var result = add(a, b);
    expect(result).toBe(-9);
  });
});

describe('presenting text', function() {
  var myText = 'this is my really cool string';
  it('should have presenter text prepended to it', function() {
    badDomInteractionTest(myText);
    var bodyText = $('body').text();
    expect(bodyText).toContain('this is the text: this is my really cool string');
  });


  it('should have presenter text prepended to it cleanly', function() {
    expect(betterTextAppendTest(myText)).toBe('this is the text: this is my really cool string');
  });
});
